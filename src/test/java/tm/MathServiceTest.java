package tm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathServiceTest {

    private MathService mathService = new MathService();

    @Test
    void sumSuccess() {
        assertEquals(20L, mathService.sum("10", "10"));
    }
    @Test
    void sumArgNotNumber() {
        assertThrows(IllegalArgumentException.class, () -> mathService.sum("a", "b"));
    }

    @Test
    void factorialSuccess() {
        assertEquals(120L, mathService.factorial("5"));
    }

    @Test
    void factorialNonPositiveArg() {
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("-10"));
    }

    @Test
    void factorialArgNotNumber() {
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial("n"));
    }

    @Test
    void factorialLongOverflow() {
        String maxInt = String.valueOf(Integer.MAX_VALUE);
        assertThrows(IllegalArgumentException.class, () -> mathService.factorial(maxInt));
    }

    @Test
    void fibonacciSuccess() {
        long[] expected = new long[] {0, 1, 1, 2, 3, 5, 8, 13, 21};
        assertArrayEquals(expected, mathService.fibonacci("54"));
    }

    @Test
    void fibonacciNoSequence() {
        assertThrows(IllegalArgumentException.class, () -> mathService.fibonacci("55"));
    }

    @Test
    void fibonacciNonPositiveArg() {
        assertThrows(IllegalArgumentException.class, () -> mathService.fibonacci("-10"));
    }

    @Test
    void fibonacciArgNotNumber() {
        assertThrows(IllegalArgumentException.class, () -> mathService.fibonacci("n"));
    }
}


