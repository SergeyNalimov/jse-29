package tm;

import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Logger;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        final MathService mathService = new MathService();

        try {
            System.out.print("Please enter command: ");
            String command = scanner.next();
            switch (command) {
                case "sum":
                    System.out.print("Please enter arg1: ");
                    String arg1 = scanner.next();
                    System.out.print("Please enter arg2: ");
                    String arg2 = scanner.next();
                    System.out.println("Sum: " + mathService.sum(arg1, arg2));
                    break;
                case "factorial":
                    System.out.print("Please enter arg: ");
                    String arg = scanner.next();
                    System.out.println("Factorial: " + mathService.factorial(arg));
                    break;
                case "fibonacci":
                    System.out.print("Please enter arg: ");
                    String fibArg = scanner.next();
                    System.out.println("Fibonacci: " + Arrays.toString(mathService.fibonacci(fibArg)));
                    break;
                default:
                    logger.warning("Unknown command");
                    break;
            }
        } catch (IllegalArgumentException e) {
            logger.severe(e.toString());
        }
    }

}



